﻿using UnityEngine;
using UnityEngine.UI;

public class StartFieldManager : MonoBehaviour
{
    [SerializeField]
    private Text _playerName;
    [SerializeField]
    private Text _placeholder;
    [SerializeField]
    private GameObject _startManager;
    [SerializeField]
    private GameObject _highscoreHolder;

    private bool _notPlaying = true;

    void Update()
    {
        if (_notPlaying)
        {
            if (Input.GetKey(KeyCode.KeypadEnter) || Input.GetKey("enter"))
            {
                StartGame();
            }

            if (Input.GetKeyDown(KeyCode.Keypad5))
            {
                ToggleVisibility();
            }
        }
    }

    private void StartGame()
    {
        string name;
        if (_playerName.text.Equals(""))
        {
            name = _placeholder.text;
        }
        else
        {
            name = _playerName.text;
        }

        Debug.Log("Set player name to " + name);

        new OnPlayerStart(name);
        _startManager.SetActive(false);
        _notPlaying = false;
    }

    private void OnDeathOfPlayer(OnPlayerDeath playerDeath)
    {
        _startManager.SetActive(true);
        _notPlaying = true;
    }

    private void Awake()
    {
        OnPlayerDeath.RegisterListener(OnDeathOfPlayer);
    }

    private void OnDestroy()
    {
        OnPlayerDeath.UnregisterListener(OnDeathOfPlayer);
    }

    private void ToggleVisibility()
    {
        if (_startManager.activeInHierarchy)
        {
            _startManager.SetActive(false);
            _highscoreHolder.SetActive(true);
        }
        else
        {
            _startManager.SetActive(true);
            _highscoreHolder.SetActive(false);
        }
    }
}
