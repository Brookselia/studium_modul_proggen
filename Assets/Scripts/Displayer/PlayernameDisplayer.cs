﻿using UnityEngine;
using UnityEngine.UI;

public class PlayernameDisplayer : MonoBehaviour
{
    [SerializeField]
    private Text _playerName;

    private void Awake()
    {
        OnPlayerStart.RegisterListener(OnSettingName);
    }

    private void OnSettingName(OnPlayerStart playerStart)
    {
        _playerName.text = playerStart.Name;
    }

    private void OnDestroy()
    {
        OnPlayerStart.UnregisterListener(OnSettingName);
    }
}
