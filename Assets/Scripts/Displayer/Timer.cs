﻿using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    [SerializeField]
    private Text _timerText;
    private float _beginTime = 0;
    private string _endTime = "";
    private bool _startTimer = false;

    private void Awake()
    {
        OnPlayerStart.RegisterListener(OnTimerBegin);
        OnPlayerDeath.RegisterListener(OnTimerStop);
    }

    private void OnTimerBegin(OnPlayerStart playerStart)
    {
        _beginTime = Time.time;
        _startTimer = true;
    }

    private void OnTimerStop(OnPlayerDeath playerDeath)
    {
        _endTime = UpdateTimer();
        _startTimer = false;
    }

    private void Update()
    {
        if (_startTimer)
        {
            _timerText.text = UpdateTimer();
        }
    }

    private string UpdateTimer()
    {
        return ((int)((Time.time - _beginTime) / 60f)).ToString("00") + ":" + ((int)((Time.time - _beginTime) % 60f)).ToString("00");
    }

    private void OnDestroy()
    {
        OnPlayerStart.UnregisterListener(OnTimerBegin);
        OnPlayerDeath.UnregisterListener(OnTimerStop);
    }
}
