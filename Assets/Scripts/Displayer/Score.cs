﻿using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{
    [SerializeField]
    private Text _scoringText;
    private int _score = 0;
    private string _playername;

    private void Awake()
    {
        OnEnemyDeath.RegisterListener(OnScoring);
        OnPlayerStart.RegisterListener(OnCountingStart);
    }

    private void Start()
    {
        UpdateScore(_score);
    }

    private void OnScoring(OnEnemyDeath death)
    {
        _score += death.Points;
        UpdateScore(_score);
    }

    private void OnCountingStart(OnPlayerStart playerStart)
    {
        _score = 0;
        _playername = playerStart.Name;
    }

    private void UpdateScore(int newScore)
    {
        if(_scoringText != null)
            _scoringText.text = newScore.ToString();
    }

    private void OnDestroy()
    {
        OnEnemyDeath.UnregisterListener(OnScoring);
        OnPlayerStart.UnregisterListener(OnCountingStart);
    }
}
