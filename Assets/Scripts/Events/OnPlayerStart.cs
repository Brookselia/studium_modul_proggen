﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlayerStart : EventCallbacks.Event<OnPlayerStart>
{
    public readonly string Name;

    public OnPlayerStart(string playerName) : base("Event on starting the game, after entering player name")
    {
        Name = playerName;
        FireEvent(this);
    }
}
