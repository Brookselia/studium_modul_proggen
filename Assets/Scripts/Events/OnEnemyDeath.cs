﻿
public class OnEnemyDeath : EventCallbacks.Event<OnEnemyDeath>
{
   public readonly int Points;


   public OnEnemyDeath(int givenPoints): base("Event, that will be fired after a enemy was killed.")
    {
        Points = givenPoints;
        FireEvent(this);
    }
}
