﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnPlayerDeath : EventCallbacks.Event<OnPlayerDeath>
{
    public OnPlayerDeath() : base("Event, that will be fired at the players death")
    {
        FireEvent(this);
    }
}
