﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [Range(5f, 20f)]
    [SerializeField]
    private float _moveSpeed = 10f;
    public float GetMoveSpeed()
    {
        return _moveSpeed;
    }

    [Range(5f, 20f)]
    [SerializeField]
    private float _rotateSpeed = 7.5f;
    public float GetRotateSpeed()
    {
        return _rotateSpeed;
    }

    private bool _canMove = false;

    private void Awake()
    {
        OnPlayerStart.RegisterListener(OnStartOfGame);
        OnPlayerDeath.RegisterListener(OnDeathOfPlayer);
    }

    void Update()
    {
        if (_canMove)
        {
            if (Input.GetKey(KeyCode.W))
                gameObject.transform.position += gameObject.transform.forward * _moveSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.S))
                gameObject.transform.position -= gameObject.transform.forward * _moveSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.A))
                gameObject.transform.Rotate(gameObject.transform.up, -0.1f * _rotateSpeed);

            if (Input.GetKey(KeyCode.D))
                gameObject.transform.Rotate(gameObject.transform.up, 0.1f * _rotateSpeed);


            if (Input.GetKey(KeyCode.Q))
                gameObject.transform.position += Vector3.Cross(gameObject.transform.forward, gameObject.transform.up) * _moveSpeed * Time.deltaTime;

            if (Input.GetKey(KeyCode.E))
                gameObject.transform.position -= Vector3.Cross(gameObject.transform.forward, gameObject.transform.up) * _moveSpeed * Time.deltaTime;
        }
    }

    private void OnStartOfGame(OnPlayerStart playerStart)
    {
        _canMove = true;
    }

    private void OnDeathOfPlayer(OnPlayerDeath playerDeath)
    {
        _canMove = false;
    }

    private void OnDestroy()
    {
        OnPlayerStart.UnregisterListener(OnStartOfGame);
        OnPlayerDeath.UnregisterListener(OnDeathOfPlayer);
    }
}
