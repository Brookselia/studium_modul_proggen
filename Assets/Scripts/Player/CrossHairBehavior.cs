﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairBehavior : MonoBehaviour
{
    private void OnAwake()
    {
        OnPlayerStart.RegisterListener(OnActivationTrue);
        OnPlayerDeath.RegisterListener(OnActivationFalse);
    }

    private void OnActivationTrue(OnPlayerStart playerStart)
    {
        this.gameObject.SetActive(true);
    }

    private void OnActivationFalse(OnPlayerDeath playerDeath)
    {
        this.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        OnPlayerStart.UnregisterListener(OnActivationTrue);
        OnPlayerDeath.UnregisterListener(OnActivationFalse);
    }
}
