﻿using System.Collections;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    [SerializeField]
    private KeyCode _shootingButton = KeyCode.Space;

    [SerializeField]
    private Camera _camera;

    [Header("Weapon Features")]
    [SerializeField]
    [Range(10f, 200f)]
    private float _range = 50f;
    [SerializeField]
    [Range(1f, 30f)]
    private float _fireRate = 5f;

    [Header("Visual Feedback")]
    [SerializeField]
    private GameObject _muzzleFlash;
    [SerializeField]
    private GameObject _impactEffect;

    private bool _canShoot = false;
    private float _nextTimeToFire = 0f;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(_shootingButton) && (Time.time >= _nextTimeToFire) && _canShoot)
        {
            _nextTimeToFire = Time.time + 1f / _fireRate;
            Shoot();
        }
    }

    private void Shoot()
    {
        if (_muzzleFlash != null)
        {
            StartCoroutine(WaitBeforeDeactivate());
        }

        RaycastHit hit;
        if(Physics.Raycast(_camera.transform.position, _camera.transform.forward, out hit, _range))
        {
            if(hit.transform.tag == "Citizen")
            {
                new OnEnemyDeath(1);
            }

            if (_impactEffect != null)
            {
                GameObject impact = Instantiate(_impactEffect, hit.point, Quaternion.LookRotation(hit.normal));
                Destroy(impact, 1f);
            }
        }
    }

    private IEnumerator WaitBeforeDeactivate()
    {
        _muzzleFlash.SetActive(true);
        yield return new WaitForSecondsRealtime(0.075f);
        _muzzleFlash.SetActive(false);
    }

    private void Awake()
    {
        OnPlayerStart.RegisterListener(OnStartOfGame);
        OnPlayerDeath.RegisterListener(OnDeathOfPlayer);
    }

    private void OnStartOfGame(OnPlayerStart playerStart)
    {
        _canShoot = true;
    }

    private void OnDeathOfPlayer(OnPlayerDeath playerDeath)
    {
        _canShoot = false;
    }

    private void OnDestroy()
    {
        OnPlayerStart.UnregisterListener(OnStartOfGame);
        OnPlayerDeath.UnregisterListener(OnDeathOfPlayer);
    }
}
