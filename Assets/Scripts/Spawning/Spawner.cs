﻿using UnityEngine;

/// <summary>
/// Client Class, initially spawns a citizen and let the citizen go to work if key is pressed.
/// </summary>
public class Spawner : MonoBehaviour
{
	/// <summary>
	/// Reference to the factory for instantiation.
	/// </summary>
	[SerializeField]
	private NPCFactory _factory;

    [Header("Spawning Options")]
	/// <summary>
	/// Reference to all possible spawn points.
	/// </summary>
	[SerializeField]
	private GameObject _spawnPoints;

    [SerializeField]
    [Tooltip("Maximale Anzahl der Einwohner")]
    [Range(1, 50)]
    private int _maxCitizen = 5;

    [SerializeField]
    [Tooltip("Dauer in s bis ein neuer Einwohner spawnt, wenn maxCitizen nicht erreicht")]
    [Range(1f, 10f)]
    private float _spawnRate = 2f;

	/// <summary>
	/// Reference to the citizen
	/// </summary>
	private GameObject _citizen;

    /// <summary>
    /// Reference to the player
    /// </summary>
    private GameObject _player;

    private int _actualCitizenCount;
    private float _nextTimeToSpawn;
    private bool _canSpawn = false;


    private void Awake()
    {
        OnEnemyDeath.RegisterListener(OnDeathOfEnemy);
        OnPlayerStart.RegisterListener(OnStartingGame);
        OnPlayerDeath.RegisterListener(OnDeathOfPlayer);
    }

    private void OnStartingGame(OnPlayerStart playerStart)
    {
        _canSpawn = true;
    }

    private void OnDeathOfPlayer(OnPlayerDeath playerDeath)
    {
        _canSpawn = false;
    }

    private void Start()
    {
        _actualCitizenCount = 0;
        _nextTimeToSpawn = _spawnRate;
    }

    /// <summary>
    /// Update is called every frame
    /// </summary>
    private void Update()
	{
        if((_actualCitizenCount < _maxCitizen) && (Time.time >= _nextTimeToSpawn) && _canSpawn)
        {
            _nextTimeToSpawn = Time.time + _spawnRate;
            SpawnCitizen();
        }
	}

    private void OnDestroy()
    {
        OnEnemyDeath.UnregisterListener(OnDeathOfEnemy);
        OnPlayerStart.UnregisterListener(OnStartingGame);
        OnPlayerDeath.UnregisterListener(OnDeathOfPlayer);
    }

    private void SpawnCitizen()
    {
        Debug.Log("Spawned Citizen");

        _actualCitizenCount++;
        _citizen = _factory.GetCitizen();
        //_citizen.transform.position = SelectSpawner(_actualCitizenCount + 1).position;
        _citizen.transform.position = SelectRandomSpawner().position;
    }

    public void OnDeathOfEnemy(OnEnemyDeath enemyDeath)
    {
        _actualCitizenCount--;
    }

    private Transform SelectRandomSpawner()
    {
        Transform[] allTransform = _spawnPoints.GetComponentsInChildren<Transform>();
        System.Random rnd = new System.Random();

        return allTransform[rnd.Next(1, allTransform.Length)];
    }

    private Transform SelectSpawner(int number)
    {
        Transform[] allTransform = _spawnPoints.GetComponentsInChildren<Transform>();
        return allTransform[number];
    }
}
