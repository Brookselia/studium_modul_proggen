﻿using UnityEngine;

public class CollisionControl : MonoBehaviour
{
    [SerializeField]
    private Transform _playerSpawner;

    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision Player and " + collision.gameObject.name);

        if (collision.gameObject.tag.Equals("Citizen"))
        {
            new OnPlayerDeath();

            gameObject.transform.position = _playerSpawner.position;
        }
    }
}
