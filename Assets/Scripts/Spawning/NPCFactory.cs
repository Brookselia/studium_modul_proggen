﻿using UnityEngine;

/// <summary>
/// Factory, spawns Prefabs/Prototypes.
/// </summary>
public class NPCFactory : MonoBehaviour
{
	/// <summary>
	/// Reference to the citizen prefab.
	/// </summary>
	[SerializeField]
	private GameObject _citizen;

    /// <summary>
    /// Function, returns a new instance of a citizen.
    /// </summary>
    /// <returns></returns>
    public GameObject GetCitizen()
	{
		return Instantiate(_citizen);
	}

}
