﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewScoreSet", menuName = "Highscore/Set")]
public class HighscoreComp : ScriptableObject
{
    public string playername;
    public int score;
}
