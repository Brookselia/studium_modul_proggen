﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newHighscoreList", menuName = "Highscore/List")]
public class PlayerScore : ScriptableObject
{
    public List<HighscoreComp> allHighscores;
}
