﻿using UnityEngine;

public class CitizenDeath : MonoBehaviour
{
    private void Awake()
    {
        OnEnemyDeath.RegisterListener(OnShot);
        OnPlayerDeath.RegisterListener(OnDeathOfPlayer);
    }

    private void OnShot(OnEnemyDeath death)
    {
        Destroy(gameObject);
    }

    private void OnDeathOfPlayer(OnPlayerDeath playerDeath)
    {
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        OnEnemyDeath.UnregisterListener(OnShot);
        OnPlayerDeath.UnregisterListener(OnDeathOfPlayer);
    }
}
