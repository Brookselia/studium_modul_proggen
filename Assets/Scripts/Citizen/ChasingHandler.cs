﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class ChasingHandler : MonoBehaviour
{
    private NavMeshAgent _navMeshAgent;
    private GameObject _player;

    // Start is called before the first frame update
    void Start()
    {
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _player = GameObject.FindGameObjectWithTag("Player");
        _navMeshAgent.speed += _player.GetComponent<PlayerMovement>().GetMoveSpeed();
    }

    // Update is called once per frame
    void Update()
    {
        MoveTo(_player);
    }

    private void MoveTo(GameObject go)
    {
        _navMeshAgent.SetDestination(go.transform.position);
    }
}
